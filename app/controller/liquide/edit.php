<?php 

if(is_post()){
	$db_return = update_liquide(

		array(
			'id' => $_POST['id'],
			'title' => $_POST['title'],
			'description' => $_POST['description'],
			'image' => $_POST['image'],
		)
	);

	if($db_return === true) $MESSAGE[] = array('type' => 'success', 'message' => 'Task method has been Updated successfully');
		else $MESSAGE[] = array('type' => 'error', 'message' => 'Task method could not been Updated.');
}

$getliquide = get_liquide_ById();