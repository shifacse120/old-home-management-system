<?php
$get_user= use_id();

// Saving the data
if(is_post()){

	$title			= get_string($_POST['title']);
	$user_id		= $_POST['user_id'];
	$description	= get_string($_POST['description']);
	
	if($title == false || $description == false ){
		$MESSAGE[] = array('type' => 'error', 'message' => 'Invalid field Requirement.');
	}
	elseif(empty($title) || empty($description) ){
		$MESSAGE[] = array('type' => 'error', 'message' => 'Field Must not be empty!.');
	}
	else{
	$db_return = add_room(
		array(
			'title'			=> $title,
			'user_id'		=> $user_id,
			'description' 	=> $description,
		)	
	);

	if($db_return === true) $MESSAGE[] = array('type' => 'success', 'message' => 'Room has been saved successfully');
		else $MESSAGE[] = array('type' => 'error', 'message' => 'Room could not been saved.');
	}
}