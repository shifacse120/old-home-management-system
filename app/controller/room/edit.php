<?php 

if(is_post()){
	$db_return = edit_room(

		array(
			'id' => $_POST['id'],
			'title' => $_POST['title'],
			'description' => $_POST['description'],
		)
	);

	if($db_return === true) $MESSAGE[] = array('type' => 'success', 'message' => 'Room has been Updated successfully');
		else $MESSAGE[] = array('type' => 'error', 'message' => 'Room could not been Updated.');
}

$getroom = get_room_ById();