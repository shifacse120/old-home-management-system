<?php

if(is_post()){
	$name			= get_string($_POST['title']);
	$description	= get_string($_POST['description']);
	$image			= $_POST['image'];
	
	if($name == false || $description == false ){
		$MESSAGE[] = array('type' => 'error', 'message' => 'Invalid field Requirement.');
	}
	elseif(empty($name) || empty($description) ){
		$MESSAGE[] = array('type' => 'error', 'message' => 'Field Must not be empty!.');
	}
	else{	
		$db_return = add_task(

			array(
				'title' => $name,
				'description' => $description,			
				'image' => $image,			
			)		
		);
		if($db_return === true) $MESSAGE[] = array('type' => 'success', 'message' => 'Task has been saved successfully');
			else $MESSAGE[] = array('type' => 'error', 'message' => 'Task could not been saved.');
	}
}