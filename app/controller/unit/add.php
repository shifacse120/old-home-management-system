<?php


// Saving the data
if(is_post()){
	$db_return = add_unit(

		array(
			'title' => $_POST['title'],
			'description' => $_POST['description'],
		)
	
	);

	if($db_return === true) $MESSAGE[] = array('type' => 'success', 'message' => 'Unit has been saved successfully');
		else $MESSAGE[] = array('type' => 'error', 'message' => 'Unit could not been saved.');
}