<?php
	
	

if(is_post()){
	


	$user_name		= get_string($_POST['name']);
	$dob			= $_POST['dob'];
	$user_role		= get_number($_POST['user_role']);
	$ssn_no			= get_string($_POST['ssn_no']);
	$email			= get_string($_POST['email']);
	$email 			= filter_var($email, FILTER_VALIDATE_EMAIL);
	$password		= $_POST['password'];

	$contact		= get_string($_POST['contact']);
	
	
	if($user_role == false || $user_name == false || $email == false || $password == false )
	{
		$MESSAGE[] = array('type' => 'error', 'message' => 'Invalid field Requirement.');
	}
	elseif(empty($user_role) || empty($user_name)  || empty($email)|| empty($password))
	{
		$MESSAGE[] = array('type' => 'error', 'message' => 'Field Must not be empty!.');
	}
	else{
	
	$db_return = add_user(

		array(
			'name'  		=> $user_name,
			'dob'  			=> $dob,
			'user_role'  	=> $user_role,
			'ssn_no'  		=> $ssn_no,
			'email'    	 	=> $email,
			'password' 	 	=> sha1($password),
			'contact' 		=> $contact,
			'image'			=> $_POST['image'],
			
			

			
		)
	
	);

	if($db_return === true) $MESSAGE[] = array('type' => 'success', 'message' => 'User has been saved successfully');
		else $MESSAGE[] = array('type' => 'error', 'message' => 'User could not been saved.');
	}
}

