<?php 

if(is_post()){
	$db_return = update_user(

		array(
			'id'			=> $_POST['id'],
			'name'  		=> $_POST['name'],
			'dob'  			=> $_POST['dob'],			
			'email'    	 	=> $_POST['email'],			
			'contact' 		=> $_POST['contact'],
			'image'			=> $_POST['image'],
			
		)
	);

	if($db_return === true) $MESSAGE[] = array('type' => 'success', 'message' => 'User Info has been Updated successfully');
		else $MESSAGE[] = array('type' => 'error', 'message' => 'User Info could not been Updated.');
}

$getUser = get_user_ById();