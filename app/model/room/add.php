<?php
add_log('model/category/add.php');

function add_room(array $data){
	
	global $db_connection;
	$table_name = get_table_name('rooms');
	$SQL = "INSERT INTO {$table_name}(title,user_id, description) VALUES('{$data['title']}','{$data['user_id']}', '{$data['description']}');";
	
	return $db_connection->query($SQL);	
}

function use_id(){
	
	global $db_connection;
	$table_name = get_table_name('users');
	$SQL = "SELECT * from {$table_name}";
	
	return $db_connection->query($SQL);	
}
function get_string($value){
	global $db_connection;
	// $check_string=mysqli_real_escape_string($db_connection,$value);	

	$value = trim($value);
  	$value = stripslashes($value);
  	$value = htmlspecialchars($value);
  	
  	$value = mysqli_real_escape_string($db_connection,$value);
  	$value = filter_var($value, FILTER_SANITIZE_STRING);
	// return is_string($value) ? $value : false;

	return $value;
}
