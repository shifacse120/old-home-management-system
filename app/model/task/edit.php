<?php

function get_task_ById(){

	global $db_connection, $PARAMETERS;
	$table_name = get_table_name('tasks');
	$id = $PARAMETERS[0];
	$SQL="SELECT * FROM {$table_name} WHERE id='{$id}'";
	return $db_connection->query($SQL);

}

function update_task(array $data){
	
	global $db_connection;
	$table_name = get_table_name('tasks');
	$SQL = "UPDATE {$table_name} SET title='{$data['title']}', description='{$data['description']}',image='{$data['image']}' WHERE id ='{$data['id']}'";
	return $db_connection->query($SQL);	

}