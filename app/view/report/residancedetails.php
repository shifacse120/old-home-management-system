   <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Residance details </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Old Home</a>
                                        </li>
                                        <li>
                                            <a href="#">Tables </a>
                                        </li>
                                        <li class="active">
                                            Residance details Tables
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

 						<div class="row">
                        	<div class="col-sm-12">
                        		<div class="card-box">
                        			

                        			<div class="table-responsive">
                        				<table id="mainTable" class="table table-striped m-b-0">
											<thead>
												<tr>
													<th>Date</th>
													<th>Time</th>
													<th>Staff Name</th>
													<th>Image</th>
													<th>Age</th>
													<th>Liquide</th>
													<th>Task</th>
													<th>Comment</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>22-11-2017</td>
													<td>10:00</td>
													<td>John sk</td>
													<td><img class="customimg" src="<?php template_url('image/angelina.jpeg');?>"></td>
													<td>32</td>
													<td>juice</td>
													<td>wash</td>
													<td>fine</td>
												</tr>
												<tr>
													<td>22-11-2017</td>
													<td>11:00</td>
													<td>Angelica ki</td>
													<td><img class="customimg" src="<?php template_url('image/we.jpeg');?>"></td>
													<td>30</td>
													<td>cofee</td>
													<td>exercise</td>
													<td>good</td>
												</tr>
												<tr>
													<td>22-11-2017</td>
													<td>10:00</td>
													<td>Yean ki</td>
													<td><img class="customimg" src="<?php template_url('image/jack.jpg');?>"></td>
													<td>90</td>
													<td>juice</td>
													<td>wash</td>
													<td>fine</td>
												</tr>
												<tr>
													<td>22-11-2017</td>
													<td>10:00</td>
													<td>Yean ki</td>
													<td><img class="customimg" src="<?php template_url('image/jack.jpg');?>"></td>
													<td>90</td>
													<td>juice</td>
													<td>wash</td>
													<td>fine</td>
												</tr>
												<tr>
													<td>22-11-2017</td>
													<td>10:00</td>
													<td>Yean ki</td>
													<td><img class="customimg" src="<?php template_url('image/jack.jpg');?>"></td>
													<td>90</td>
													<td>juice</td>
													<td>wash</td>
													<td>fine</td>
												</tr>
												<tr>
													<td>22-11-2017</td>
													<td>10:00</td>
													<td>Yean ki</td>
													<td><img class="customimg" src="<?php template_url('image/jack.jpg');?>"></td>
													<td>90</td>
													<td>juice</td>
													<td>wash</td>
													<td>fine</td>
												</tr>
											</tbody>
											
										</table>
                        			</div>
                        		</div>
                        	</div>
                        </div>

              </div>
          </div>
      </div>