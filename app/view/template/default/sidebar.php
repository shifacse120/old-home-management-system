<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
              <li class="menu-title">Navigation</li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span> </a>
                    
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-receipt"></i><span> Report </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?c=report&a=staff"><i class="ti-info-alt"></i>Staff </a></li>
                        
                        <li><a href="index.php?c=report&a=residance"><i class="ti-info-alt"></i>Residence</a></li>
                        
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="index.php?c=visit&a=add"><i class="fa fa-tripadvisor"></i><span> Visit </span> </a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user"></i><span> User </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?c=user&a=add"><i class="fa fa-plus"></i>Add User</a></li>
                        
                        <li><a href="index.php?c=user&a=view"><i class="fa fa-eye"></i>View User</a></li>
                        
                    </ul>
                </li>
                
                


         
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-tint"></i><span> Liquide </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?c=liquide&a=add"><i class="fa fa-plus"></i>Add Liquide</a></li>
                        
                        <li><a href="index.php?c=liquide&a=view"><i class="fa fa-eye"></i>View Liquide</a></li>
                        
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bed"></i><span> Room </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?c=room&a=add"><i class="fa fa-plus"></i>Add Room</a></li>
                        
                        <li><a href="index.php?c=room&a=view"><i class="fa fa-eye"></i>View Room</a></li>
                        
                    </ul>
                </li>
                 <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class=" ti-ruler-alt"></i><span> Unit </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?c=unit&a=add"><i class="fa fa-plus"></i>Add Unit</a></li>
                       
                        <li><a href="index.php?c=unit&a=view"><i class="fa fa-eye"></i>View Unit</a></li>
                        
                    </ul>
                </li>
             
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-tasks"></i><span> Task </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?c=task&a=add"><i class="fa fa-plus"></i>Add Task</a></li>
                       
                        <li><a href="index.php?c=task&a=view"><i class="fa fa-eye"></i>View Task</a></li>
                        
                    </ul>
                </li>
               

               

            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

        

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
