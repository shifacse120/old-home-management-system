<?php
add_log('view/user/add.php');
?>


<!-- Start right Content here -->
            <!-- ============================================================== -->
<div class="content-page">
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit User </h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="#">Old Home</a>
                        </li>
                        <li>
                            <a href="#">User </a>
                        </li>
                        <li class="active">
                            Edit User
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-xs-12">
                <div class="card-box">

                    <div class="row">
                        <div class="col-sm-12 col-xs-12 col-md-6">

                            <div class="p-20">
                                <?php
                                    if($getUser){
                                        while ($data=$getUser->fetch_assoc()) {?>
                    
                                            <form action="" method="post" data-parsley-validate novalidate>
                                                <input type='hidden' value='<?php echo $data['id'];?>' name='id'>
                                                <div class="form-group">
                                                    <label for="user-title">User Name<span class="text-danger">*</span></label>
                                                    <input type="text" value="<?php echo $data['name'];?>" name="name" parsley-trigger="change" required class="form-control" id="user-title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="user-title">Date of Birth<span class="text-danger">*</span></label>
                                                    <input type="text" value="<?php echo $data['dob'];?>" name="dob" parsley-trigger="change" required class="form-control" id="user-title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pro-price">Email <span class="text-danger">*</span></label>
                                                    <input type="text" name="email" value="<?php echo $data['email'];?>" parsley-trigger="change" required class="form-control" id="pro-price">
                                                </div>
                                        
                                                
                                                <div class="form-group">
                                                    <label for="">contact<span class="text-danger">*</span></label>
                                                    <input type="text" name="contact" value="<?php echo $data['contact'];?>" parsley-trigger="change" required  class="form-control" id="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="company_name">Image<span class="text-danger">*</span></label>
                                                    <input type="file" name="image" value="<?php echo $data['image'];?>" parsley-trigger="change"   class="form-control" id="">
                                                </div>
                                               


                                                <div class="form-group text-right m-b-0">
                                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                        Update User
                                                    </button>
                                                    
                                                </div>

                                            </form>
                                <?php

                                        }
                                    }
                                ?>
                            </div>

                        </div>

                
                    </div>
                    <!-- end row -->

                </div> <!-- end ard-box -->
            </div><!-- end col-->

        </div>
        <!-- end row -->


    </div> <!-- container -->

</div> <!-- content -->
