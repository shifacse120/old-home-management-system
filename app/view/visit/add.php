<div class="container step">
  
<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Step 1</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
        <p>Step 2</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Step 3</p>
      </div>
    </div>
  </div>
  
  <form role="form" action="" method="post">
    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Step 1</h3>
          <div class="form-group">
            <label for="user-title">Residance Id<span class="text-danger">*</span></label>
            <select class="form-control" name="user_role"  required="required" >
              <option value="-1">select one</option>
              <option value="1">Admin</option>
              <option value="2">staff</option>
              <option value="3">residance</option>
            </select>
          </div>
          <div class="form-group">
              <label for="user-title">Staff Id<span class="text-danger">*</span></label>
              <select class="form-control" name="user_role"  required="required" >
                <option value="-1">select one</option>
                <option value="1">Admin</option>
                <option value="2">staff</option>
                <option value="3">residance</option>
              </select>
          </div>
          <div class="form-group">
            <label class="control-label">Date<span class="text-danger">*</span></label>
            <input type="" name="" required="required" class="form-control datepicker"  >
          </div>
          <div class="form-group">
            <label class="control-label">Time<span class="text-danger">*</span></label>
            <select class="form-control" name="user_role"  required="required" >
                <option value="-1">select one</option>
                <option value="00:00">00:00</option>
                <option value="01:00">01:00</option>
                <option value="02:00">02:00</option>
                <option value="03:00">03:00</option>
                <option value="04:00">04:00</option>
                <option value="05:00">05:00</option>
                <option value="06:00">06:00</option>
                <option value="07:00">07:00</option>
                <option value="08:00">08:00</option>
                <option value="09:00">09:00</option>
                <option value="10:00">10:00</option>
                <option value="11:00">11:00</option>
                <option value="12:00">12:00</option>
                <option value="13:00">13:00</option>
                <option value="14:00">14:00</option>
                <option value="15:00">15:00</option>
                <option value="16:00">16:00</option>
                <option value="17:00">17:00</option>
                <option value="18:00">18:00</option>
                <option value="19:00">19:00</option>
                <option value="20:00">20:00</option>
                <option value="21:00">21:00</option>
                <option value="22:00">22:00</option>
                <option value="23:00">23:00</option>
                <option value="24:00">24:00</option>
                
                
              </select>
          </div>
            <div class="form-group">
              <label for="user-title">Status<span class="text-danger">*</span></label>
              <select class="form-control" name="user_role"  required="required" >
                <option value="-1">select one</option>
                <option value="1">WNPU</option>
                <option value="2">WPU</option>
                <option value="3">DNP</option>
                <option value="4">DPU</option>
              </select>
          </div>
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-2">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Step 2</h3>
          <div class="form-group">
            <label class="control-label"><p style="font-weight: bold;">Liquide<span class="text-danger">*</span></p></label>
            
              <div class="funkyradio col-md-12">
                  
                  <div class="funkyradio-success col-md-3">
                      <input type="checkbox" name="checkbox" id="checkbox1" />
                      <label for="checkbox1">coffee</label>
                  </div>
                  <div class="funkyradio-success col-md-3">
                      <input type="checkbox" name="checkbox" id="checkbox2" />
                      <label for="checkbox2">juice</label>
                  </div>
                  <div class="funkyradio-success col-md-3">
                      <input type="checkbox" name="checkbox" id="checkbox3" />
                      <label for="checkbox3">toothpaste</label>
                  </div>
                  
              </div>  
          </div>

          <div class="form-group">

            <label class="control-label"><br>Comment </label>
            <textarea  type="text" required="required" class="form-control" ></textarea>
          </div>
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-3">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Step 3</h3>
          <div class="form-group">
            <label class="control-label"><p style="font-weight: bold;">Task</p></label>
            <div class="funkyradio col-md-12">
                  
                  <div class="funkyradio-success col-md-3">
                      <input type="checkbox" name="checkbox" id="checkbox4" />
                      <label for="checkbox4">Wash</label>
                  </div>
                  <div class="funkyradio-success col-md-3">
                      <input type="checkbox" name="checkbox" id="checkbox5" />
                      <label for="checkbox5">Food</label>
                  </div>
                  <div class="funkyradio-success col-md-3">
                      <input type="checkbox" name="checkbox" id="checkbox6" />
                      <label for="checkbox6">exercise</label>
                  </div>
                  
                  
            </div> 
          </div>
          <div class="form-group">
            <label class="control-label"><br>Comment</label>
            <textarea  type="text" required="required" class="form-control" placeholder="comment here" ></textarea>
          </div>
          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
        </div>
      </div>
    </div>
  </form>
  
</div>