-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2017 at 10:15 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `old_home`
--

-- --------------------------------------------------------

--
-- Table structure for table `liquides`
--

CREATE TABLE `liquides` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `liquides`
--

INSERT INTO `liquides` (`id`, `title`, `description`, `image`, `createTime`, `updateTime`) VALUES
(2, 'fbdsfbdf', 'dfbdfbdfbdfvdfvdfvfvf', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'sfdvdfvdf', 'vsfvsfvsdvsddavsdvsdvdsvsdv', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `description`, `image`, `createTime`, `updateTime`) VALUES
(2, 'dfbn', 'dsfbfgdhbfgngfjnfhgnhn', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `title`, `description`, `createTime`, `updateTime`) VALUES
(2, 'gvc nfgb', 'g gf fg fg gf dvdsvdsvsd', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'f fd fsd', 'dsdsds', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_role` int(11) NOT NULL,
  `ssn_no` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `user_role`, `ssn_no`, `name`, `dob`, `email`, `password`, `contact`, `image`, `createTime`, `updateTime`) VALUES
(5, 0, 2, '', 'Shifa Alam', '1994-11-17', 'himel@gmail.com', '174be25d5d722f9c55463fc69b432a167eb51d22', 'dvfvfbf', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 0, 1, '', 'himel', '2017-11-08', 'himel@gmail.com', '602e9eb2d44e5dd1b6d87f730bca4ee527cc70ec', '01928', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 0, 3, '', 'ridoy haq', '2017-11-02', 'himel@gmail.com', 'fb96549631c835eb239cd614cc6b5cb7d295121a', 'dc', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 0, 2, '7890', 'Bunny', '2017-11-23', 'bony@gmail.com', 'fb96549631c835eb239cd614cc6b5cb7d295121a', '019843432223', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 0, 2, '7890', 'fgbfgng', '2017-11-02', 'himel@gmail.com', 'fb96549631c835eb239cd614cc6b5cb7d295121a', '09u7', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 0, 2, '7890', 'fgbfgng', '2017-11-02', 'himel@gmail.com', 'fb96549631c835eb239cd614cc6b5cb7d295121a', '09u7', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 0, 1, '7890', 'ridoy haq', '2017-11-02', 'himel@gmail.com', 'fb96549631c835eb239cd614cc6b5cb7d295121a', '1234', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `visits`
--

CREATE TABLE `visits` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `visits_liquide`
--

CREATE TABLE `visits_liquide` (
  `id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `liquide_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `visit_tasks`
--

CREATE TABLE `visit_tasks` (
  `id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `liquides`
--
ALTER TABLE `liquides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visits_liquide`
--
ALTER TABLE `visits_liquide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_tasks`
--
ALTER TABLE `visit_tasks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `liquides`
--
ALTER TABLE `liquides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `visits`
--
ALTER TABLE `visits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `visits_liquide`
--
ALTER TABLE `visits_liquide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `visit_tasks`
--
ALTER TABLE `visit_tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
